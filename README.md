# Installation
1. Install npm by commandline >> npm install
2. Install cypress on your machine by commandline >> npm install cypress
3. Install project from this repository by commandline >> git clone 

# Running project with different URLs
1. Go to the project folder by commandline >> cd /{path}/{this_project_folder}
2. Open Snabbare project by commandline >> npx cypress open --config baseUrl=https://www.snabbare.com/
3. Run the pom_comeon.tests.js file
4. Run Hajper project by commandline >> npx cypress open --config baseUrl=https://www.hajper.com/
5. Run the pom_comeon.tests.js file
4. If you want to run one of the projects use commandline >> npx cypress run --config baseUrl={this_is_project_url}

# Notes
1. File with name raw_comeon.tests.js contains the same code in raw view
2. Code is reusable for both projects
3. There weas used the Paje Object pattern

# Reports
1. I didn't implement any reporters but there is an [article](https://docs.cypress.io/guides/tooling/reporters#Examples) with detailed explanation how to install one of them.
2. Also you can run the projects and see report about pass/fails of specs