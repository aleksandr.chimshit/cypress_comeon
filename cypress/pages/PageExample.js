/// <reference types="cypress" />


class PageExample {
    constructor() {

    }

    randomAmount() {
        return parseInt(Math.floor(Math.random() * (200 - 100 + 1) + 100))
    }

    clickPutPlayButton() {
        cy.get('[data-at="deposit-and-play-homepage"]').focus().click()
    }

    sentButtonIsDisable() {
        cy.get('[data-at="deposit-login-submit-button-noaccount"]')
        .should('be.disabled')
    }

    fillAmount(currencyAmount) {
        cy.get('[data-at="amount-input-label"]').type(currencyAmount)
    }

    clickSentButton() {
        cy.get('[data-at="deposit-login-submit-button-noaccount"]')
        .should('be.enabled')
        .click()
    }

    selectSwishPayment() {
        cy.get('[data-at="SWISH-paymentmethod-deposit-and-play"]')
        .click() 
    }

    bankPageIsVisible() {
        cy.get('[data-at="sidebar-layout"]')
        .should('be.visible')
    }

    clickBankCancelButton() {
        cy.get('[data-at="bankid-loader-cancel"]').click()
    }

    verifySelectPaymentPage() {
        cy.url()
        .should('include', 'guest-deposit-login')
        
        cy.get('[data-at="SWISH-paymentmethod-deposit-and-play"]')
        .should('be.visible')
    }

    inputPersonalId(id) {
        cy.get('[data-at="input-personalNumber"]')
        .type(id)
    }

    clicktBankIdButton() {
        cy.get('[data-at="register-login-bankid-button"]').click()
    }

    verifyBankIdForm(id) {
        cy.url()
        .should('include', 'guest-deposit-login')

        cy.get('[data-at="input-personalNumber"]')
        .should('have.value', id)
    }
    
    
}

export default PageExample;