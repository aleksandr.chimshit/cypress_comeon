/// <reference types="cypress" />

/*
It's a test module which based on the Page Object pattern
I created it according to the programming principles
for avoiding boilerplate code. But it still can be improved.
*/

import ConfigProject from '../fixtures/ConfigProject'
import PageExample from '../pages/PageExample'

Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
  });

const conf = new ConfigProject();
const pageEx = new PageExample();

const userId = 198608051903
const currencyAmount = 200


describe('Mobile tests suit', () => {
    beforeEach(() => {
        conf.setMobileConfigs('iphone-xr')
        conf.visitPage()
    })

    it('Open bank page and come back', () => {
        pageEx.clickPutPlayButton()

        pageEx.sentButtonIsDisable()

        pageEx.fillAmount(currencyAmount)

        pageEx.clickSentButton()

        pageEx.selectSwishPayment()

        pageEx.bankPageIsVisible()

        pageEx.clickBankCancelButton()

        pageEx.verifySelectPaymentPage()
    })

    afterEach(() => {
        cy.logout
    })

})

describe('Desktop tests suit', () => {
    beforeEach(() => {
        conf.setDesktopConfigs()
        conf.visitPage()
    })

    it('Open bank page, use ID and come back', () => {
        pageEx.clickPutPlayButton()

        pageEx.sentButtonIsDisable()

        pageEx.fillAmount(currencyAmount)

        pageEx.clickSentButton()

        pageEx.selectSwishPayment()

        pageEx.inputPersonalId(userId)

        pageEx.clicktBankIdButton()

        pageEx.bankPageIsVisible()

        pageEx.clickBankCancelButton()

        pageEx.verifyBankIdForm(userId)
    })

    afterEach(() => {
                cy.logout
            })
})