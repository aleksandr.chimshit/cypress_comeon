/// <reference types="cypress" />

/*
It's a test module which is not more than raw tests 
for comparing with another variant.
Lines of code 134 vs 85
*/

Cypress.on('uncaught:exception', (err, runnable) => {
    return false;
  });


describe('Mobile tests suit', () => {

    beforeEach(() => {
        // mobile view
        cy.viewport('iphone-xr')

        /* You might see black screen when you visit the site with Cypress. Inorder to
        overcome this, add cookie "integrationtesting" with value as "true" in the before
        method */        
        cy.setCookie('integrationtesting', 'true')

        // Handle Cookie Content Popup (can be handled on Cookie level before the test)
        cy.setCookie('CookieConsent', 'false')

        // Go to Landing Page "www.hajper.com" or "www.snabbare.com"
        cy.visit('/')
    })

    it('Open bank page and come back', () => {

        //Click "Satt in och spela" button.
        cy.get('[data-at="deposit-and-play-homepage"]').focus().click()

        // Verify "Satt in" button is disabled.
        cy.get('[data-at="deposit-login-submit-button-noaccount"]')
        .should('be.disabled')

        // Enter an amount 200
        cy.get('[data-at="amount-input-label"]').type('200')

        // Click "Satt in" button
        cy.get('[data-at="deposit-login-submit-button-noaccount"]')
        .should('be.enabled')
        .click()

        // Choose "Swish" as payment method.
        cy.get('[data-at="SWISH-paymentmethod-deposit-and-play"]')
        .click()

        // Verify "Start Bank ID" page is visible
        cy.get('[data-at="sidebar-layout"]')
        .should('be.visible')

        // Click "Avbryt" button to cancel
        cy.get('[data-at="bankid-loader-cancel"]').click()

        // Verify User goes back to Payment Method Selection page
        cy.url()
        .should('include', 'guest-deposit-login')
        
        cy.get('[data-at="SWISH-paymentmethod-deposit-and-play"]')
        .should('be.visible')
    })

    afterEach(() => {
        cy.logout
    })

})

describe('Desktop tests suit', () => {
    beforeEach(() => {
        // desktop view
        cy.viewport(1920, 1024)
        /* You might see black screen when you visit the site with Cypress. Inorder to
        overcome this, add cookie "integrationtesting" with value as "true" in the before
        method */   
        cy.setCookie('integrationtesting', 'true')
        // Handle Cookie Content Popup (can be handled on Cookie level before the test)
        cy.setCookie('CookieConsent', 'false')
        // Go to Landing Page "www.hajper.com" or "www.snabbare.com"
        cy.visit('/')
    })

    it('Open bank page, use ID and come back', () => {
        //Click "Satt in och spela" button.
        cy.get('[data-at="deposit-and-play-homepage"]').focus().click()

        // Verify "Satt in" button is disabled.
        cy.get('[data-at="deposit-login-submit-button-noaccount"]')
        .should('be.disabled')

        // Enter an amount 200
        cy.get('[data-at="amount-input-label"]').type('200')

        // Click "Satt in" button
        cy.get('[data-at="deposit-login-submit-button-noaccount"]')
        .should('be.enabled')
        .click()

        // Choose "Swish" as payment method.
        cy.get('[data-at="SWISH-paymentmethod-deposit-and-play"]')
        .click()

        // Enter Personal Number "198608051903"
        cy.get('[data-at="input-personalNumber"]')
        .type('198608051903')

        // Click "Bank ID" button
        cy.get('[data-at="register-login-bankid-button"]')
        .click()

        // Verify "Start Bank ID" page is visible
        cy.get('[data-at="sidebar-layout"]')
        .should('be.visible')

        // Click "Avbryt" button to cancel
        cy.get('[data-at="bankid-loader-cancel"]').click()

        // Verify User goes back to Enter Personal Number page
        cy.url()
        .should('include', 'guest-deposit-login')

        cy.get('[data-at="input-personalNumber"]')
        .should('have.value', '198608051903')
    })

    afterEach(() => {
                cy.logout
            })
})