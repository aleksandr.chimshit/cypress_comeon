/// <reference types="cypress" />


class ConfigProject {
    constructor() {

    }

    visitPage() {
        cy.visit('/')
    }
    
    setMobileConfigs(device) {
        cy.viewport(device)
        cy.setCookie('integrationtesting', 'true')
        cy.setCookie('CookieConsent', 'false')
    }

    setDesktopConfigs() {
        cy.viewport(1920, 1024)
        cy.setCookie('integrationtesting', 'true')
        cy.setCookie('CookieConsent', 'false')
    }
}

export default ConfigProject;